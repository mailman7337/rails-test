require 'rails_helper'

feature 'Post stream' do
  before(:each) do
    visit('/')
  end

  scenario 'when User has a post and is signed in' do
    user = create(:user_with_posts)
    sign_in_with user.email, "P@55word"

    expect(page).to have_selector(:link_or_button, 'Edit')
  end

  scenario 'when a post has a More tag' do
    create(:post, :with_more_tag)
    visit('/')
    expect(page).to have_selector(:link_or_button, 'Continue Reading')
  end

  scenario 'navigated by tags' do
    create(:post, :with_more_tag)
    visit('/')
    within('#tag-navigation') do
      click_link "HACKER NEWS"
    end

    expect(page).to have_content("Posts tagged with \"hacker news\"")
  end

  scenario 'navigated by author' do
    create(:post, :with_more_tag)
    visit('/')
    first(:link, "Glenn Tucker").click

    expect(page).to have_content("Posts by Glenn Tucker")
  end 
end