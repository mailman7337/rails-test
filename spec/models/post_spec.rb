require 'rails_helper'

describe Post do
  before(:each) do
    @post = build(:post)
    @proposal = build(:post, :a_modest_proposal)
  end

  it "has a valid factory" do
    expect(@post).to be_valid
  end

  it "is invalid without a title" do
    @post.title = nil
    expect(@post).not_to be_valid
  end

  it "knows who its author is" do
    expect(@proposal.author.first_name).to eq("Adam")
  end
  
  it "knows if a given user is its author" do
    @adam = User.where({:first_name => "Adam"}).first
    expect(@proposal.authored_by?(@adam)).to eq(true)
  end

  it "knows if a given user is not its author" do
    @bob = build(:user, :bob)
    expect(@proposal.authored_by?(@bob)).to eq(false)
  end

  it "knows if it's an external post" do
    expect(@proposal.external?).to eq(true)
  end

  it "knows if it's not an external post" do
    expect(@post.external?).to eq(false)
  end

  it "knows if its content contains a 'More' tag" do
    expect(@proposal.has_more_tag?).to eq(true)
  end

  it "knows if its content does not contain a 'More' tag" do
    expect(@post.has_more_tag?).to eq(false)
  end

  it "can return its content with the 'More' tag(s) removed" do
    expect(@proposal.without_more_tags.index(/<!--\s*more\s*-->/i)).to be_nil
  end
  
  it "can return its excerpt" do
    # check last word before the split, and make sure it's "for"
    expect(@proposal.excerpt.split[-1]).to eq("for")
  end
end