require 'rails_helper'

feature 'Standard application layout' do
  before(:all) do
    create(:user_with_posts)
  end

  before(:each) do
    visit('/')
  end

  scenario 'User is signed in' do
    sign_in
    expect(page).to have_content('SIGN OUT')
    expect(page).to have_content('Signed in as')
  end

  scenario 'User is a visitor' do
    expect(page).to have_content(Post.last.tag_list[0].upcase)
    expect(page).to have_content('SIGN UP')
  end
end