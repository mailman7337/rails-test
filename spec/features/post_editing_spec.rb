require 'rails_helper'

feature 'Post editing' do
  before(:each) do
    sign_in
    @user = User.last
    @post = @user.posts[0]
    visit("/edit/#{@post.id}")
  end

  scenario 'when user is signed in' do
    expect(page).to have_content('Options')
    expect(page).to have_content('Delete')
  end

  scenario 'when user changes post tags' do
    fill_in "post_tag_list", :with => "New, Tags"
    click_button 'Save'
    
    expect(page).to have_content("Post updated successfully")
  end

  scenario 'when user is not signed in' do
    sign_out
    visit("/edit/#{@post.id}")

    # Redirect to sign-in page
    expect(page).to have_content("You need to sign in")
  end

  scenario 'when signed-in user is not the author of the post' do
    @post = create(:post, :a_modest_proposal)
    visit("/edit/#{@post.id}")

    # Redirect to sign-in page
    expect(page).to have_content("you aren't the author!")
  end

  scenario 'when author deletes post' do
    click_link 'Delete'

    expect(page).to have_content("Post successfully deleted!")
  end
end