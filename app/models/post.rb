class Post < ActiveRecord::Base
  belongs_to :user

  validates :title, presence: true
  validates :slug, presence: true, uniqueness: true
  acts_as_url :title, :url_attribute => :slug
  acts_as_taggable

  default_scope order('created_at desc')

  scope :by_user_id, ->(user_id) { where("user_id = ?", user_id) }

  def authored_by?(a_user)
    a_user.id == user.id
  end

  def author
    user
  end

  def to_param
    slug
  end

  def external?
    url.present?
  end

  def has_more_tag?
    content =~ more_tag_pattern ? true : false
  end

  def without_more_tags
    content.gsub(more_tag_pattern,'')
  end

  def excerpt
    if content.index(more_tag_pattern)
      content.split(more_tag_pattern)[0]
    else
      content
    end
  end

  private

  def more_tag_pattern
    /<!--\s*more\s*-->/i
  end
end
