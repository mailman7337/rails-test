require 'rails_helper'

feature 'Visitor signs up' do
  scenario 'with valid first and last name, email, and password' do
    sign_up_with 'Example', 'User', 'valid@example.com', 'password'

    expect(page).to have_content('SIGN OUT')
    expect(page).to have_content('ADMIN')
  end

  scenario 'with invalid first name' do
    sign_up_with '', 'User', 'invalid_email', 'password'

    expect(page).to have_content('SIGN IN')
  end

  scenario 'with invalid last name' do
    sign_up_with 'Example', '', 'invalid_email', 'password'

    expect(page).to have_content('SIGN IN')
  end

  scenario 'with invalid email' do
    sign_up_with 'Example', 'User', 'invalid_email', 'password'

    expect(page).to have_content('SIGN IN')
  end

  scenario 'with blank password' do
    sign_up_with 'Example', 'User', 'valid@example.com', ''

    expect(page).to have_content('SIGN IN')
  end
end