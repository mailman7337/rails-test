require 'rails_helper'

feature 'User signs in' do
  before(:each) do
    create(:user)
  end

  scenario 'with correct email and password' do
    sign_in_with 'adam@example.com', 'P@55word'

    expect(page).to have_content('Signed in successfully.')
  end

  scenario 'with empty user name' do
    sign_in_with '', 'P@55word'

    expect(page).to have_content('Invalid email or password.')
  end

  scenario 'with empty password' do
    sign_in_with 'adam@example.com', ''

    expect(page).to have_content('Invalid email or password.')
  end

  scenario 'with incorrect password' do
    sign_in_with 'adam@example.com', 'password'

    expect(page).to have_content('Invalid email or password.')
  end
end