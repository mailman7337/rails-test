require 'rails_helper'

feature 'Post authoring' do
  before(:each) do
    visit('/new')
  end

  scenario 'User is signed in' do
    sign_in
    expect(page).to have_content('Options')
    expect(page).to have_content('Delete')
  end

  scenario 'User writes a draft post' do
    sign_in
    fill_in "post_title", :with => "Test Draft Post"
    click_button 'Save'
    visit('/admin')
    within("#drafts") do
      expect(page).to have_content('Test Draft Post')
    end
  end

  scenario 'User writes a published post' do
    sign_in 
    fill_in "post_title", :with => "Test Published Post"
    uncheck 'post_draft'
    click_button 'Save'
    visit('/admin')
    within("#published") do
      expect(page).to have_content(Post.first.title)
    end
  end

  scenario 'User is a visitor' do
    # Redirect to sign-in page
    expect(page).to have_content('SIGN IN')
  end
end