FactoryGirl.define do  
  factory :post do
    title "Codename: TestApp"
    tag_list "test post, first, hacker news"
    content "Earlier today I was browsing Hacker News and was a bit let down by a post I ran across by recent HN front-pager"
    draft false
    
    trait :second_post do
      title "Codename: TestApp #2"
      tag_list "second"
      content "Dummy content"
    end

    trait :a_modest_proposal do
      title "A Modest Proposal"
      slug "a-modest-proposal"
      tag_list "first, guest post, swift, publick"
      content "...for Preventing the Children of Poor People From Being a Burden on Their Parents or Country, and for <!-- more -->Making Them Beneficial to the Publick"
      url "http://jswift.io"
      draft true
      association :user, :factory => :user
    end

    trait :with_more_tag do
      content "This one has a...<!-- more --> more tag. Below the fold"
      association :user, :factory => :user
    end

    factory :random_post do
      title { Faker::Lorem.words(4).join(' ') }
      tag_list { Faker::Lorem.words(5).join(',') }
      content { Faker::Lorem.sentences(10) } 
      draft false
    end
  end
end