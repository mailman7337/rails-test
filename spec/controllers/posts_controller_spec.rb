require 'rails_helper'

RSpec.describe PostsController, :type => :controller do
  let!(:published) { create(:post) }
  let!(:draft) { create(:post, :a_modest_proposal) }

  let(:valid_attributes) {
    { :slug => 'a-tale-of-two-cities',
      :title => 'A Tale of Two Cities',
      :content => 'It was the best of times',
      :draft => false }
  }

  let(:invalid_attributes) {
    { :slug => 'an-invalid-post',
      :title => nil,
      :content => nil,
      :draft => false }
  }

  describe 'GET #index' do
    it 'lets anyone view all published posts' do
      @second_post = create(:post, :second_post) 
      get :index
      expect(assigns(:posts).size).to eq(2)
    end

    it 'does not include draft posts' do
      get :index
      expect(assigns(:posts).size).to eq(1)
    end

    it 'renders the :index view' do
      get :index
      expect(response).to render_template :index
    end
  end

  describe 'GET #admin' do
    login_user 

    it 'renders for signed-in users' do
      get :admin
      expect(response).to be_success
    end

    it 'renders the :admin view' do
      get :admin
      expect(response).to render_template :admin
    end
  end

  describe 'GET #show' do
    it 'lets anyone read a post' do
      get :show, :slug => 'codename-testapp'
      expect(response).to be_success
    end

    it 'does not let spies read a draft post' do
      get :show, :slug => 'a-modest-proposal'
      expect(response).to have_http_status(404)
    end
  end

  describe 'DELETE #destroy' do
    login_user

    it 'allows signed-in users to delete their posts' do
      expect {
        delete :destroy, :slug => 'a-modest-proposal'
      }.to change(Post, :count).by(-1)
    end
  end

  describe 'GET #edit' do
    login_user

    let(:post) { Post.find_by_slug('a-modest-proposal') }

    it 'renders for signed-in users' do
      get :edit, :id => post.id
      expect(response).to be_success
    end

    it 'renders the :edit view' do
      get :edit, :id => post.id
      expect(response).to render_template :edit
    end
  end

  describe 'GET #new' do
    login_user

    it 'renders for signed-in users' do
      get :new
      expect(response).to be_success
    end

    it 'renders the :new view' do
      get :new
      expect(response).to render_template :new
    end
  end

  describe 'POST #create' do
    login_user

    context 'with valid attributes' do
      it 'allows logged-in users to create a post' do
        expect {
          post :create, :post => valid_attributes
        }.to change(Post, :count).by(1)
      end

      it 'redirects user to the edit page' do
        post :create, :post => valid_attributes
        expect(response).to redirect_to("/edit/#{Post.first.id}")
      end
    end

    context 'with invalid attributes' do
      it 'does not allow users to create an invalid post' do
        expect {
          post :create, :post => invalid_attributes
        }.to change(Post, :count).by(0)
      end

      it 'redirects user to the edit page' do
        post :create, :post => invalid_attributes
        expect(response).to render_template :new
      end
    end
  end

  describe 'PUT #update' do
    login_user

    context 'with valid attributes' do
      before(:each) do
        put :update, :slug => 'a-modest-proposal', :post => valid_attributes
      end

      it 'located the correct Post' do
        expect(assigns(:post)).to eq(draft)
      end

      it 'changes the Post attributes' do
        draft.reload
        expect(draft.title).to eq("A Tale of Two Cities")
        expect(draft.slug).to eq('a-tale-of-two-cities')
        expect(draft.content).to eq('It was the best of times')
      end

      it 'redirects user to the edit page' do
        expect(response).to redirect_to("/edit/#{draft.id}")
      end
    end

    context 'with invalid attributes' do
      it 'does not change Post attributes' do
        put :update, :slug => 'a-modest-proposal', :post => invalid_attributes
        expect(draft.title).to eq("A Modest Proposal")
      end
    end
  end
end