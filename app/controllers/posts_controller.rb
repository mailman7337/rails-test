class PostsController < ApplicationController
  before_filter :authenticate_user!, :except => [:index, :show]
  layout :choose_layout

  def index
    if params[:user]
      @filter_user = User.find(params[:user])
      @posts = Post.by_user_id(@filter_user)
    elsif params[:tag]
      @tag = params[:tag]
      @posts = Post.tagged_with(@tag)
    else
      @posts = Post
    end
    @posts = @posts.page(params[:page]).per(10).where(draft:false)

    respond_to do |format|
      format.html
      format.xml { render :xml => @posts }
      format.rss { render :layout => false }
    end
  end

  def preview
    @post = Post.new(params[:post])
    @preview = true
    @post.user = current_user
    respond_to do |format|
      format.html { render 'show' }
    end
  end

  def admin
    @no_header = true
    @post = Post.new
    @published = Post.by_user_id(current_user.id).where(draft:false).page(params[:post_page]).per(20)
    @drafts = Post.by_user_id(current_user.id).where(draft:true).page(params[:draft_page]).per(20)

    respond_to do |format|
      format.html
    end
  end

  def show
    @single_post = true
    @post = admin? ? Post.find_by_slug(params[:slug]) : Post.find_by_slug_and_draft(params[:slug],false)

    respond_to do |format|
      if @post.present?
        format.html
        format.xml { render :xml => @post }
      else
        format.any { head status: :not_found  }
      end
    end
  end

  def new
    @no_header = true
    @posts = Post.page(params[:page]).per(20)
    @post = Post.new

    respond_to do |format|
      format.html
      format.xml { render xml: @post }
    end
  end

  def edit
    @no_header = true
    @post = Post.find(params[:id])

    unless current_user && @post.authored_by?(current_user)
      respond_to do |format|
        format.html { redirect_to '/', :alert => "You can't edit posts for which you aren't the author!" }
      end
    end
  end

  def create
    @post = Post.new(params[:post])
    @post.user_id = current_user.id

    respond_to do |format|
      if @post.save
        format.html { redirect_to "/edit/#{@post.id}", :notice => "Post created successfully" }
        format.xml { render :xml => @post, :status => :created, location: @post }
      else
        format.html { render :action => 'new' }
        format.xml { render :xml => @post.errors, :status => :unprocessable_entity}
      end
    end
  end

  def update
    @post = Post.find_by_slug(params[:slug])

    respond_to do |format|
      if current_user && @post.authored_by?(current_user)
        if @post.update_attributes(params[:post])
          format.html { redirect_to "/edit/#{@post.id}", :notice => "Post updated successfully" }
          format.xml { head :ok }
        else
          format.html { render :action => 'edit', :alert => "There were some errors." }
          format.xml { render :xml => @post.errors, :status => :unprocessable_entity}
        end
      else
        format.html { redirect_to '/', :alert => "You can't update posts for which you aren't the author!"}
      end
    end
  end

  def destroy
    @post = Post.find_by_slug(params[:slug])

    respond_to do |format|
      if current_user && @post.authored_by?(current_user)
        @post.destroy
        format.html { redirect_to '/admin', :notice => "Post successfully deleted!" }
        format.xml { head :ok }
      else
        format.html { redirect_to '/', :alert => "You can't delete posts for which you aren't the author!" }
      end
    end
  end

  private

  def admin?
    session[:admin] == true
  end

  def choose_layout
    if ['admin', 'new', 'edit', 'create'].include? action_name
      'admin'
    else
      'application'
    end
  end
end
