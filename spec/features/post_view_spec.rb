require 'rails_helper'

feature 'Post view' do
  before(:each) do
    post = create(:post, :with_more_tag)
    sign_in_with post.author.email, "P@55word"

    visit("/#{post.to_param}")
  end

  scenario 'author is signed in' do
    expect(page).to have_selector(:link_or_button, 'Edit')
  end

  scenario 'when a post has a More tag' do
    expect(page).to have_content('Below the fold')
    expect(page).to_not have_content('<!-- more -->')
  end
end