class ApplicationController < ActionController::Base
  protect_from_forgery
  before_filter :get_tags

  private

  def get_tags
      # return all tags sorted by descending frequency
      @tags = Post.tag_counts_on(:tags)
                    .sort_by { |tag| tag[:taggings_count] }
                    .reverse
  end
end
