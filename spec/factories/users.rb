FactoryGirl.define do  
  factory :user do
    first_name   "Adam"
    last_name    "Tucker"
    email         "adam@example.com"
    password     "P@55word"
    password_confirmation "P@55word"

    trait :bob do
      first_name "Bob"
      email       "bob@example.com"
      password    "secretpass"
      password_confirmation "secretpass"
    end

    # user_with_posts will create post data after the user has been created
    factory :user_with_posts do
      first_name   "Glenn"
      last_name    "Tucker"
      email         { Faker::Internet.email }
      password     "P@55word"
      password_confirmation "P@55word"
      
      transient do
        posts_count 5
      end

      after(:create) do |user, evaluator|
        create_list(:random_post, evaluator.posts_count, user: user)
      end
    end
  end
end