Ruby on Rails Coding Test
========================

The purpose of this coding test is to test for the ability
* to work with existing code
* to write clean, maintainable code
* to implement new features in according to existing coding style
* to approach tasks creatively


1. Implement user accounts
--------------------------

Users should be able to come to the site and register new user accounts.


*Your comments:*
> I started here first. I used the magnificent `devise` gem to implement user accounts. My implementation is complete with user sign-up, sign-in, sign-out, password reset, lockout, and unlock. All post controller actions except `:show` and `:index` require login to access.





2. Users can login to admin interface
-------------------------------------

The admin section should be accessible via login after creation of an account.


*Your comments:*
> Once logged in, the user "Admin" link appears in the bottom-left of the main layout. When users click this, they are taken to the admin portal. Here, they are of course only able to edit posts for which they are the author. They may also add new posts.




3. Logged in user can edit their user page
------------------------------------------

Add a link to the admin panel to the user page. On the user page, you can edit your account details.



*Your comments:*
> Account details are available via the "Edit User" link in the top-right of the Admin page. Here, they are able to edit all User attributes (first name, last name, email & password for now).




4. Add username to all blog posts
---------------------------------

All blog posts should show the name of the user who wrote them.


*Your comments:*
> Each post (regardless of where it's seen) now includes a byline. The end-user may click the name of the author to see all articles submitted by that author (via a scope used in the posts#index action).




5. Users can tag their posts
----------------------------

Users should be able to add tags to their blog posts. All tags that are used on the site should be visible on the side bar and user should be able to navigate the site using them.

*Your comments:*
> Here I used one of my favorite gems - `acts_as_taggable_on`. Each post may be tagged with a set of comma-separated terms or phrases. On the left bar of the main layout, the user may see all tags used on the site. Each tag is a link, which leads to a filtered index page, showing only pages tagged with that tag. 


Thanks for your interest - good luck!



Adam's Overall Comments
-------------------

For each alteration I made, I took specific care to:

1. Match existing coding style (e.g. hashrockets instead of the ruby 1.9.3 hash associations), and
2. Match the site look-and-feel. Some styles were mocked after existing styles (e.g. buttons), while others were brand new, but kept with the overall site look-and-feel (e.g. the tag input field).

I avoided refactoring of small or large scale, but I did make a few alterations to existing code to increase consistency:

* I changed the Post model's `has_more_tag` method to read `has_more_tag?`. This makes it more consistent with Ruby conventions, and also matches the convention of the other boolean-returning method provided by the Post model - `external?`.

* I made the Post model code a little "dry-er" by separating the RegExp pattern for the "more" tag (which was repeated several times) into its own private method, `more_tag_pattern`.

* For css classes and ids, the author of the simple blog switched often between snake_case and hyphenated-names. I decided to match whichever convention was used closest to the new tags I was inserting. 

Even though many tests were written in the Rails built-in framework, I included a totally separate test suite which tests Models, Controllers (lite testing), and integration (with Capybara).

