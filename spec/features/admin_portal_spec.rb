require 'rails_helper'

feature 'Admin portal' do
  before(:each) do
    visit('/admin')
  end

  scenario 'User is signed in' do
    sign_in
    expect(page).to have_content('Drafts')
    expect(page).to have_content('Edit User')
    expect(page).to have_content('Home')
  end

  scenario 'Drafts exist' do
    sign_in
    click_link 'New Draft'
    fill_in "post_title", :with => "Test Draft Post"
    click_button 'Save'
    visit('/admin')
    within("#drafts") do
      expect(page).to have_content('Test Draft Post')
    end
  end

  scenario 'User has posts' do
    sign_in 
    within("#published") do
      expect(page).to have_content(Post.first.title)
    end
  end

  scenario 'User is a visitor' do
    expect(page).to have_content('Remember me')
  end
end