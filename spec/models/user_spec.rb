require 'rails_helper'

describe User do
  before(:each) do
    @user = build(:user)
    @bob = build(:user, :bob)
  end

  it "has a valid factory" do
    expect(@user).to be_valid
  end

  it "is invalid without a first name" do
    @user.first_name = nil
    expect(@user).not_to be_valid
  end

  it "is invalid without a last name" do
    @user.last_name = nil
    expect(@user).not_to be_valid
  end

  it "is invalid without an email address" do
    @user.email = nil
    expect(@user).not_to be_valid
  end

  it "can return its full name" do
    expect(@user.full_name).to eq("#{@user.first_name} #{@user.last_name}")
  end

  it "authenticates with the correct password" do 
    expect(@bob.valid_password?("secretpass")).to eq(true)  
  end

  it "fails to authenticate with the incorrect password" do
    expect(@bob.valid_password?("incorrect")).to eq(false)
  end
end