module Features
  module SessionHelpers
    def sign_up_with(first_name, last_name, email, password)
      visit('/users/sign_up')
      fill_in 'First name', :with => first_name
      fill_in 'Last name', :with => last_name
      fill_in 'Email', :with => email
      fill_in 'Password', :with => password
      fill_in 'Password confirmation', :with => password
      click_button 'SIGN UP'
    end

    def sign_in_with(email, password)
      visit new_user_session_path
      fill_in 'Email', :with => email
      fill_in 'Password', :with => password
      click_button 'SIGN IN'
    end

    def sign_in
      user = create(:user_with_posts)
      visit new_user_session_path
      fill_in 'Email', :with => user.email
      fill_in 'Password', :with => user.password
      click_button 'SIGN IN'
    end

    def sign_out
      visit("/")
      click_link 'sign-out'
    end
  end
end

